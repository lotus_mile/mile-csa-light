#ifndef MILECSA_MILECSA_LIGHT_API_UTILS_HPP
#define MILECSA_MILECSA_LIGHT_API_UTILS_HPP

#include <string>
const std::string StringFormat(const char* format, ...);
const std::string ErrorFormat(const char* format, ...);

#endif // MILECSA_MILECSA_LIGHT_API_UTILS_HPP